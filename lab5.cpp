#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"
using namespace std;
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

typedef struct Card {
  Suit suit;
  int value;
} Card;

string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}


int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
  srand(unsigned (time(0)));

  /*Create a deck of cards of size 52 (hint this should be an array) and
   *initialize the deck*/
  Card deck[52];
  for(int i = SPADES; i <= CLUBS; i++) {
    for(int j = 0; j < 13; j++) {
      deck[j + (13 * (int)i)].suit = (Suit)i;
      deck[j + (13 * (int)i)].value = j+2;
    }
  }

  /*After the deck is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/
  random_shuffle(deck, &deck[52], myrandom);

  /*Build a hand of 5 cards from the first five cards of the deck created
   *above*/
  Card drawn[5] = {deck[0], deck[1], deck[2], deck[3], deck[4]};

  /*Sort the cards.  Links to how to call this function is in the specs
   *provided*/
  sort(drawn, &drawn[5], suit_order);

  /*Now print the hand below. You will use the functions get_card_name and
   *get_suit_code */
  for(int i = 0; i < 5; i++) {
    cout << setw(10) << get_card_name(drawn[i]) << " of "
	 << get_suit_code(drawn[i]) << endl;
  }

  return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
bool suit_order(const Card& lhs, const Card& rhs) {

  /*Test for suit order (Sp > Ht > Dm > Cl)
   *Return true if out of order*/
  if((lhs.suit < rhs.suit)) {
    return true;
  }

  /*Test for value order when suits are in order
   *Return true if out of order*/
  if((lhs.suit == rhs.suit) && (lhs.value < rhs.value)) {
    return true;
  }

  return false;
}

string get_suit_code(Card& c) {
  switch (c.suit) {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
  }
}

string get_card_name(Card& c) {
  switch (c.value) {
    case 11:
      return "Jack";
    case 12:
      return "Queen";
    case 13:
      return "King";
    case 14:
      return "Ace";
    default:
      return to_string(c.value);
  }
}
